#!/bin/sh

DEST_DIR=$2

fatal() {
	echo "(PB EXTRAS FETCHER) [FATAL!]" "$@" >&2
        exit 1
}

say() {
    echo -e "(PB EXTRAS FETCHER) " $@ >&2;
}

warn() {
    echo -e "(PB EXTRAS FETCHER) [WARN] " "$@" >&2;
}

fetch_one_die() {
    fn_url="$1"
    (curl -s -f -R -O "$fn_url") || \
		fatal "File could not be downloaded from:" $fn_url ".Aborting";
    
}

fetch_one_warn() {
    fn_url="$1"
    (curl -s -f -R -O "$fn_url") || \
		warn "File could not be downloaded from:" $fn_url " .";
}


fetch() {
    
    FN_SRC=$(readlink -f "$1")
    status=$?
    DEST_DIR=$2


    [ "${status}" -ne 0 ] && fatal "Error. The file containing the list of files to be
 downloaded does not exist.  "

    cd "${DEST_DIR:-.}"
    say Downloading source files to "${DEST_DIR}"
    while read fn; do
	# Check for md5 hashes
	bfn=$(basename "$fn")	# Base source filename.
	mdfn="${bfn}.md5"	# MD5 filename corresponding to the source filename.
	rm -f "${mdfn}"
	say  "Downloading hash file: ${fn}.md5"
	fetch_one_warn "${fn}.md5" # Download MD5, or warn if it does not exist.
	
	if [ -e "$bfn" ] && [ -e "${mdfn}" ]; then
	    
	    md5sum -c --status "$mdfn" && \
		say  "Skipping unchanged file $fn ." && \
		     continue
	fi
	
	say  "Downloading $fn"
	fetch_one_die "$fn"
	md5sum -c --status "$mdfn" || fatal "(fetch): Error: Download of $fn corrupted. Abort."
    done < "$FN_SRC";

}


if [ -f "${DEST_DIR}/__DONOTSTART__" ]; then
    say "Found lockfile in DEST_DIR. Not doing anything."
    exit 0
else
    touch "${DEST_DIR}/__DONOTSTART__"
    fetch "$1" "$2"
    rm "${DEST_DIR}/__DONOTSTART__"    
fi

exit 0
